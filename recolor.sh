#!/bin/bash

# Ansi color code variables
# shellcheck disable=SC2034
red="\e[0;91m"
# shellcheck disable=SC2034
blue="\e[0;94m"
# shellcheck disable=SC2034
green="\e[0;92m"
# shellcheck disable=SC2034
white="\e[0;97m"
# shellcheck disable=SC2034
bold="\e[1m"
# shellcheck disable=SC2034
uline="\e[4m"
# shellcheck disable=SC2034
reset="\e[0m"

# I don't actually know if global variables are reset after running the script...so I manually reset them
originalPATH="$PATH$"
export PATH="$PATH:$PWD/gum"

## elementaryRecolor

set -e

function installTheme() {
	if [ ! $(which ninja) ] && [ ! $(which sassc) ] && [ ! $(which msgfmt) ];then
		echo "build dependencies not found! run $0 --prep first!"
		exit 1;
	fi
	echo -e "${green}==> Entering build dir...${reset}"
	cd stylesheet/
	meson build --prefix=/usr
	cd build
	echo -e "${green}==> Cleaning previous installs/builds...${reset}"
	ninja clean
	echo -e "${green}==> Installing...${reset}"
	sudo ninja install
	echo -e "${green}==> All Done! You might need to reboot, or logout once.${reset}"
}

function patch() {
	echo -e "${green}==> Please select a file to use from the following list${reset}"
	mkdir -p patched-themes
	selectedTheme="$(gum choose $(ls patched-themes))"
	cp -r "patched-themes/$selectedTheme" "stylesheet/src/gtk-3.0/_palette.scss"
	cp -r "patched-themes/$selectedTheme" "stylesheet/src/gtk-4.0/_palette.scss"
	installTheme
}

function help() {
	echo -e "${green}${bold}Syntax${reset}:  $0 <options>"
	echo -e "${green}${bold}options${reset}:"
	echo -e "${blue}$0 --help			Shows this help page${reset}"
	echo -e "${blue}$0 --reset			Restores ElementaryOS default theming${reset}"
	echo -e "${blue}$0 --reset			Restores ElementaryOS default theming${reset}"
	echo -e "${blue}$0 --flatpak			Attempts to make Flatpak applications work with the stylesheets without re-install${reset}"
	echo -e "${blue}$0 --prep			Installs build dependencies ${red} - RUN THIS FIRST!${reset}"
	echo -e "${blue}$0 --ninjafix			Works around the ninja log permission issue${reset}"
}

# The implementation for this is experimental, and might not work in production
function flatpakFix() {
	flatpak override --filesystem=/usr/share/themes:ro
	flatpak override --filesystem=~/.themes:ro
	flatpak override --filesystem=xdg-config/gtk-3.0:ro
	flatpak override --filesystem=xdg-config/gtk-4.0:ro
}

function reset() {
	echo -e "${green}==> Resetting submodules...${reset}"
	git submodule deinit --all -f
	git submodule init
	git submodule update --force
	echo -e "${green}==>  re-installing original themes from source${reset}"
	installTheme
}

function buildPrep() {
	echo -e "${green}==> Installing build dependencies...${reset}"
	apt install meson ninja-build sassc gettext -y
	echo -e "${green}==> Build dependencies installed!${reset}"
}

function buildLogFix() {
	echo -e "${green}==> Removing old ninja build logs...${reset}"
	sudo rm stylesheet/build/.ninja_log -rf
	echo -e "${green}==> Done! Make sure to try again!${reset}"
}
case $@ in
	--help)
		help;;
	--flatpak)
		flatpakFix;;
	--reset)
		reset;;
	--prep)
		buildPrep;;
	--ninjafix)
		buildLogFix;;
	*)
		patch;;
esac

export PATH="$originalPATH"
